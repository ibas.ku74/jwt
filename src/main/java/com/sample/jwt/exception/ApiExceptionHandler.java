package com.sample.jwt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = ApiRequestException.class)
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {

        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        var exception = ApiException.builder()
                .message(e.getMessage())
                .httpStatus(badRequest)
                .timeStamp(ZonedDateTime.now(ZoneId.of("Asia/Bangkok")))
                .build();

        return new ResponseEntity<>(exception, badRequest);
    }
}

