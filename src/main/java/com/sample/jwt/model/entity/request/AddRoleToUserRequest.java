package com.sample.jwt.model.entity.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddRoleToUserRequest {
    private String username;
    private String roleName;
}
